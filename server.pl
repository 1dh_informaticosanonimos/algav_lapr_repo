:- dynamic linhas/1.
:- dynamic encomenda/2.
:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.
:- dynamic tarefa/6.
:- dynamic tarefas/1.
:- dynamic clientes/1.
:- dynamic prioridade_cliente/2.
:- dynamic operacoes_produto/2.
:- dynamic produtos/1.
:- dynamic maquinas/1.
:- dynamic tipos_maq_linha/2.
:- dynamic tipo_operacoes/1.
:- dynamic operacao_maquina/5.
:- dynamic agenda_maq/2.
:- dynamic planoFabrico/3.
:- dynamic popIdeal/1.
% Linhas

linhas([]).


% Maquinas


maquinas([]).


% Ferramentas


ferramentas([fa,fb,fc,fd,fe,ff,fg,fh,fi,fj]).


% Maquinas que constituem as Linhas

tipos_maq_linha(lA,[]).

% ...


% Opera��es

tipo_operacoes([]).

% operacoes/1 vai ser criado dinamicamente
%no exemplo dara' uma lista com 30 operacoes 6 lotes de produtos * 5 operacoes por produto

%operacoes_atrib_maq/2 vai ser criado dinamicamente
%no exemplo cada maquina tera' 6 operacoes atribuidas, uma por cada lote de produtos

% classif_operacoes/2 deve ser criado dinamicamente
%no exemplo teremos 30 factos deste tipo, um para cada operacao


% Afeta��o de tipos de opera��es a tipos de m�quinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,1,1).



%Clientes

clientes([]).


% prioridades dos clientes


%...


% PRODUTOS

produtos([]).



% ENCOMENDAS


% ...

% Encomendas do cliente,
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

% ...

% op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)

% -------------------- MÉTODO GERAIS ------------------------- %

geraEscalonamentoComDadosReais(Data):- geraDadosSemOutput(Data), cria_op_enc, cria_tarefas_enc, numeroLotes(Numero),((Numero > 4,!,
			geraSemInput(50,10, 0.35, 0.05,150, -1, 45));(geraEscalonamentoBF(_))),popIdeal(MelhorPopFinal),geraEscalonamentoLotesTarefas(MelhorPopFinal), melhorarEscalonamento, geraTempoFinalExec(TempoF),
			((planoFabrico(Id,_,TempoF),!,retractall(planoFabrico(_,_,_)),IdNovo is Id+1, assertz(planoFabrico(IdNovo, MelhorPopFinal,TempoF)));
			(assertz(planoFabrico(0, MelhorPopFinal,TempoF)))).


geraEscalonamento(Pop, Avaliacao):- cria_op_enc, cria_tarefas_enc,  geraSemInput(100,80, 0.35, 0.03,150 , 0, 80), popIdeal(Pop), avalia(Pop, Avaliacao), geraEscalonamentoLotesTarefas(Pop).

geraEscalonamentoBruteForce(Pop, Avaliacao):- cria_op_enc, cria_tarefas_enc,  geraBruteForce(Pop, Avaliacao).

geraEscalonamentoBF(Pop):- geraBruteForce(Pop, _), criaPopIdealSemAvaliacao(Pop).

melhorarEscalonamento:- removerSetupsDesnecessarios, deslizarAgendasEsquerda(_).

criaPopIdealSemAvaliacao(Pop):-(popIdeal(_),!,retract(popIdeal(_)),assertz(popIdeal(Pop));(assertz(popIdeal(Pop)))).
criaPopIdeal(Pop):- tiraAvaliacao(Pop, PopF),(popIdeal(_),!,retract(popIdeal(_)),assertz(popIdeal(PopF));(assertz(popIdeal(PopF)))).

% --------------------- BRUTE FORCE --------------------------------%
% Gera a melhor sequencia de lotes através de um find all. Descobre
% todas as permutações dos lotes e de seguida compara-os todos,
% descobrindo o melhor através da sua avaliação (soma de atrasos
% pesados).
geraBruteForce(MelhorLote, Tempo):- getListaLotes(Lotes), findall(Permutacoes, permuta(Lotes, Permutacoes),ListaPerm), melhorLote(ListaPerm,  MelhorLote, Tempo).

% Compara os lotes através de avaliação.
melhorLote([], "lote1", 90000000).
melhorLote([Lote|Tail], Melhor, Avaliacao):- melhorLote(Tail, Melhor1, Avaliacao1), avalia(Lote, AvaliacaoTemp), ((AvaliacaoTemp < Avaliacao1,!, Melhor = Lote, Avaliacao is AvaliacaoTemp);(Avaliacao is Avaliacao1, Melhor = Melhor1)).

%Descobre lista inicial de lotes
getListaLotes(ListaLotes):- findall(Lotes, tarefa(_,Lotes,_,_,_,_), ListaLotes).

%Conta numero de lotes
numeroLotes(Num):- getListaLotes(Lotes), contaLotes(Lotes, Num).

contaLotes([], 0).
contaLotes([_|Tail], Sum):- contaLotes(Tail, Sum1), Sum is Sum1 + 1.
% --------------------- DESLIZES E REMOVER SETUPS ------------------%

% Vai buscar o deslize mais pequeno através do predicado
% getDeslizeMaisPequeno2/2 e desliza as agendas através do predicado
% deslizarAgendas/2.
deslizarAgendasEsquerda(Deslize):- findall(Maquinas, agenda_maq(Maquinas,_), Lista), getDeslizeMaisPequeno2(Lista,Deslize), deslizarAgendas(Lista, Deslize).

% Percorre cada máquina e apaga o primeiro e segundo
% elemento(guardando-os) visto que o ponto de deslize é entre o 2º e 3º
% termo. A partir do 3º termo, vai deslizar todos os tempos iniciais e
% finais menos o tempo de deslize.
deslizarAgendas([],_).
deslizarAgendas([Maq|Tail], Deslize):- deslizarAgendas(Tail, Deslize),agenda_maq(Maq, Lista), apagaPrimeiroSegundo(Lista, ListaF, ListaElemsApagados), deslizaTempos(ListaF, Deslize, NovaLista), retract(agenda_maq(Maq, _)), append(ListaElemsApagados, NovaLista, ListaFinal), assertz(agenda_maq(Maq, ListaFinal)).

% Subtrai os tempos iniciais e finais com o deslize - pode tanto ser um
% setup como exec.
deslizaTempos([], _, []).
deslizaTempos([t(Ti,Tf,setup,[Ferr])|Tail], Deslize, [t(NovoTi,NovoTf, setup, [Ferr])|NovaLista]):- NovoTi is Ti - Deslize, NovoTf is Tf - Deslize, deslizaTempos(Tail, Deslize, NovaLista).
deslizaTempos([t(Ti,Tf,exec,[Op, Qtd, Prod, Cli, Tarefa])|Tail], Deslize, [t(NovoTi,NovoTf,exec, [Op,Qtd,Prod,Cli,Tarefa])|NovaLista]):- NovoTi is Ti - Deslize, NovoTf is Tf - Deslize, deslizaTempos(Tail, Deslize, NovaLista).

% Percorre todas as máquinas e respetiva agenda, descobrindo a mais
% pequena.
getDeslizeMaisPequeno2([],999).
getDeslizeMaisPequeno2([Maq|Tail], Deslize):- getDeslizeMaisPequeno2(Tail, Deslize1), agenda_maq(Maq, Lista), apagaPrimeiro(Lista, ListaAgenda), getDeslizeMaquina(ListaAgenda, DeslizeTemp), ((DeslizeTemp < Deslize1,!, Deslize is DeslizeTemp);(Deslize is Deslize1)).

getDeslizeMaquina([H|Tail], Deslize):- getDeslizeMaquina2(H, Tail, Deslize).

% Deslize é a diferença do primeiro tempo final menos o segundo tempo
% inicial (deslize 2º e 3º termo).
getDeslizeMaquina2(t(_,Tf,exec,[_,_,_,_,_]), [t(Ti,_,_,[_])|_], Deslize):-!, Deslize is Ti - Tf.
getDeslizeMaquina2(t(_,Tf,exec,[_,_,_,_,_]), [t(Ti,_,exec,[_,_,_,_,_])|_], Deslize):-!,Deslize is Ti-Tf.
getDeslizeMaquina2(t(_,Tf,setup,[_]), [t(Ti,_,_,[_])|_], Deslize):-!, Deslize is Ti - Tf.
getDeslizeMaquina2(t(_,Tf,setup,[_]), [t(Ti,_,exec,[_,_,_,_,_])|_], Deslize):-!,Deslize is Ti-Tf.


% Elimina todas as ocorrências de um setup com a mesma ferramenta, após
% o primeiro.

apagaSetup(F, [], [F]):-!.
apagaSetup(t(Ti,Tf,setup,[F]), [t(_,_,setup,[F])|L], L1):-!,apagaSetup(t(Ti,Tf,setup,[F]), L, L1).
apagaSetup(X, [Y|L], [Y|L1]):- apagaSetup(X,L,L1).


% Remover Setups desnecessários

removerSetupsDesnecessarios:- findall(Maquinas, agenda_maq(Maquinas,_), Lista), removerSetupsMaquinas(Lista).

% Percorrer cada máquina e remove os setups desnecessários das agendas
% temporais.
removerSetupsMaquinas([]).
removerSetupsMaquinas([Maq|Tail]):- removerSetupsMaquinas(Tail),agenda_maq(Maq, ListaProducao),
			retiraSetups(ListaProducao, ListaSetup, ListaExec),
			remove_duplicates(ListaSetup, LS),removeSetupsRepetidos(LS,LS, LF),
			reverse(ListaExec, ListaExecReverse),
			append(LF, ListaExecReverse, ListaAppend),
			sort(ListaAppend, ListaSort),
			retract(agenda_maq(Maq,_)),
			assertz(agenda_maq(Maq,ListaSort)).

% Vai remover os vários setups repetidos da mesma ferramenta - percorre
% recursivamente - os primeiros setups serão analisados primeiro por
% isso os últimos é que serão removidos.
removeSetupsRepetidos([], L, L):-!.
removeSetupsRepetidos([Op|Tail], ListaSetups, ListaFinal):- removeSetupsRepetidos(Tail, ListaSetups, ListaFinal1), ((member(Op, ListaFinal1),!,
			apagaSetup(Op, ListaSetups, ListaFinal));(ListaFinal = ListaFinal1)).


% Divide os termos de produção da agenda em duas listas - os de setup e
% os de execução.
retiraSetups([], [], []).
retiraSetups([t(Ti, Tf,setup, [Ferr])|Tail], [t(Ti,Tf,setup, [Ferr])|Lista], ListaExec):- retiraSetups(Tail,Lista, ListaExec).
retiraSetups([t(Ti, Tf,exec, [Op,Qtd,Prod,Cl,Tarefa])|Tail], Lista, [t(Ti,Tf,exec,[Op,Qtd,Prod,Cl,Tarefa])|ListaExec]):- retiraSetups(Tail, Lista, ListaExec).


% --------------------------- AGENDAS TEMPORAIS --------------------- %

% Gera escalonamento pós A.G.
cria_agenda_maq_setup(Maq, Ti, Tf, Ferr):- ((agenda_maq(Maq,Lista),!, retract(agenda_maq(Maq,Lista)), assertz(agenda_maq(Maq, [t(Ti,Tf,setup,[Ferr])|Lista])));(assertz(agenda_maq(Maq, [t(Ti, Tf, setup, [Ferr])])))).

cria_agenda_maq_exec(Maq, Ti, Tf, Op, Qtd, Prod, Cl, IdTarefa):- atom_string(Prod,ProdS), atom_string(Cl, ClS),  ((agenda_maq(Maq,Lista),!, retract(agenda_maq(Maq,Lista)), assertz(agenda_maq(Maq, [t(Ti,Tf,exec,[Op, Qtd, ProdS, ClS, IdTarefa])|Lista])));(assertz(agenda_maq(Maq, [t(Ti, Tf, exec, [Op, Qtd, ProdS, ClS, IdTarefa])])))).

% Recebe a lista de lotes para escalonar, criando posteriormente as
% agendas com a máquina.
geraEscalonamentoLotesTarefas(ListaTarefas):-retractall(agenda_maq(_,_)), reverse(ListaTarefas,ListaT), geraEscalonamentoLotesTarefas2(ListaT,_).

% Vai percorrendo os lotes e criando as agendas temporais tarefa a
% tarefa, tendo em conta sempre o makespan total atual(anterior).
geraEscalonamentoLotesTarefas2([], 0).
geraEscalonamentoLotesTarefas2([Tarefa|Tail], MakespanAnt):- geraEscalonamentoLotesTarefas2(Tail, Makespan1), tarefa(_,Tarefa,Makespan,_,_,_),geraEscalonamentoTarefa(Tarefa,Makespan1), MakespanAnt is Makespan.

% Vai buscar a operação inicial da produção do produto, criando a agenda
% temmporal para essa operação. Após, cria as restantes agendas
% anteriores a essa operação e posteriores, usando o método
% eliminaItemListas/4 para ir buscar as mesmas.
geraEscalonamentoTarefa(IdTarefa, MakespanAnterior):- tarefa(_,IdTarefa,_,_,_,Prod),
			getOperacaoInicialMakespan(Prod, Op), op_prod_client(Op, Maq, Ferr, Prod,Cl,Qtd,_,Setup,Exec),
			TExec is Setup + (Exec*Qtd) + MakespanAnterior,
			TExecI is MakespanAnterior + Setup,
			cria_agenda_maq_exec(Maq, TExecI, TExec, Op, Qtd, Prod, Cl, IdTarefa),
			TSetup is Setup + MakespanAnterior,
			cria_agenda_maq_setup(Maq,MakespanAnterior, TSetup, Ferr),
			eliminaItemListas(Prod,Op, ListaSemOp, ListaAntesOp),
			reverse(ListaSemOp, ListaSemOpRev), FirstExec is MakespanAnterior+Setup+Exec,
			FirstSetup is Setup + MakespanAnterior,
			geraEscalonamentoAnteriorPrimeiraOp(FirstSetup, ListaAntesOp,_,IdTarefa),
			geraRestoEscalonamento(FirstExec, ListaSemOpRev, _,_, IdTarefa).


eliminaItemListas(Prod,OpEliminar, ListaSemOp, ListaAntesOp):- findall(Op, op_prod_client(Op,_,_,Prod,_,_,_,_,_), ListaOps),
			eliminaItem(OpEliminar, ListaOps, ListaSemOp),
			getItemsAntesElem(OpEliminar, ListaOps, ListaAntesOp).

% O Escalonamento posterior é calculado através do tempo de execução
% anterior -> o setup: Ti = (tempo de execução anterior da primeira
% operação - o setup) Tf = Ti + Setup
% -> a execução é o Ti = TempoExecução anterior Tf =(tempo de execução
% anterior da primeira operação + o tempo de execução * quantidade). Se
% o tempo de execução de uma operação for mais pequeno que o anterior, o
% cálculo do tempo de execução atual é diferente -> Tf = (TempoExecução
% ultima operação anterior + TempoExecução ultima operação atual).
geraRestoEscalonamento(TExec, [], TExec, 0, _).
geraRestoEscalonamento(TExecAtual, [Op|Tail], TExec, TExecAnterior, IdTarefa):-
			geraRestoEscalonamento(TExecAtual, Tail, TExec1, TExecAnterior1, IdTarefa),
			op_prod_client(Op, Maq, Ferr, Prod,Cl,Qtd,_,Setup,Exec), TSetup is TExec1 - Setup,
			((Exec < TExecAnterior1,!, TTemp is TExecAnterior1 + Exec);(TTemp is TExec1 + (Exec*Qtd))),
			cria_agenda_maq_exec(Maq, TExec1, TTemp,Op,Qtd, Prod,Cl,IdTarefa), TExec is TExec1 + Exec, TExecAnterior is TTemp,
			cria_agenda_maq_setup(Maq, TSetup, TExec1, Ferr).

% O Escalonamento anterior é semelhante ao posterior, sendo que se usa
% os tempos posteriores. Para o setup ->Ti: (Tempo de execução da
% primeira operação atual - setup)Tf : Ti + Setup. Para a execução ->
% Ti=(Tempo de Setup da operação posterior - Tempo de Execução), Tf =
% Setup Operação posterior.
geraEscalonamentoAnteriorPrimeiraOp(TExec,[], TExec, _).
geraEscalonamentoAnteriorPrimeiraOp(TExecAtual, [Op|Tail], TExec, IdTarefa):-
			geraEscalonamentoAnteriorPrimeiraOp(TExecAtual, Tail, TExec1, IdTarefa),
			op_prod_client(Op, Maq, Ferr, Prod,Cl,Qtd,_,Setup,Exec),
			TExecInicial is TExec1 - Exec,
			TSetInicial is TExecInicial - Setup,
			TSetTemp is TSetInicial + Setup,
			cria_agenda_maq_setup(Maq, TSetInicial, TSetTemp, Ferr),
			TExec is TExec1,
			TExecTemp is TExecInicial + (Exec*Qtd),
			cria_agenda_maq_exec(Maq,TExecInicial, TExecTemp,Op, Qtd, Prod, Cl,IdTarefa).

%[t(0, 60, setup, ["f1"])
geraTempoFinalExec(Tempo):- agenda_maq(_, Lista), retiraSetups(Lista, _, ListaExec), maiorElementoAgenda(Tempo,ListaExec).
% --------------------------- CRIAÇÃO TAREFAS ---------------------- %

% Facto que vai criar as tarefas através da encomenda. Primeiramente,
% procura as encomendas e respetivos tempos, ordenando por ordem de
% tempo de conclusão(Earliest Due Date). Após ordenar por ordem
% crescente, é reversed para criar as tarefas recursivamente.
cria_tarefas_enc:- findall(p(TConc,lote, Makespan, Peso, ProdLotes), (encomenda(Cliente, ListaLotes), retiraProdLotes(ListaLotes, ProdLotes),calculaMakespanLista(ListaLotes, Makespan), calculaTConc(ListaLotes,TConc), prioridade_cliente(Cliente, Peso)), ListaTermos), sort(ListaTermos, ListaSortTConc), reverse(ListaSortTConc, ListaReverse), iniciaCriarTarefas(ListaReverse,0).

% Retira o nome dos produtos dos lotes.
retiraProdLotes([],[]).
retiraProdLotes([e(P,_,_)|Tail], [P|Lista]):- retiraProdLotes(Tail, Lista).

% Calcula tempo conclusão.
calculaTConc([], []).
calculaTConc([e(_,_, TConc)|Tail], [TConc|Lista]):- calculaTConc(Tail, Lista).

% Calcula makespan do lote.
calculaMakespanLista([], []).
calculaMakespanLista([e(Produto,_,_)|Tail], [Makespan|Lista]):- calculaMakespanLista(Tail, Lista), calculaMakespan(Produto, Makespan).

% Retract nas tarefas já existentes.
iniciaCriarTarefas(ListaFinal, N):- retractall(tarefas(_)), retractall(tarefa(_,_,_,_,_,_)), criarTarefas(ListaFinal, N, NTarefas), assertz(tarefas(NTarefas)).

% Criação do facto tarefas.
criarTarefas([], N,N).
criarTarefas([p(ListaTConc,Id, ListaMakespans, Prioridade, ListaProds)|Tail], N, NFinal):- criarTarefasAux(ListaTConc,Id, ListaMakespans,ListaProds, Prioridade, N,NAt), criarTarefas(Tail, NAt, NFinal).

% Ao criar as tarefas, verifica quais as linhas que possam ser
% utilizadas para o fabrico do produto - após ter essa lista de linhas,
% descobre qual a menos carregada.
criarTarefasAux([TConc],Id, [Make],[Prod], Prioridade, N, N1):-N1 is N+1,
			verificaLinhaProd(Prod,Linhas),
			((isEmpty(Linhas),!,true);
			(getLinhaMenosCarregada(Linhas, Linha, _), atomic_concat(Id,N1,Nome), assertz(tarefa(TConc, Nome, Make, Prioridade,Linha, Prod)))).


criarTarefasAux([TConc|Tail],Id,[Make|TailC],[Prod|TailP],Prioridade, N,NAt):-criarTarefasAux(Tail,Id, TailC,TailP, Prioridade,N,NTemp),N1 is NTemp+1,NAt is NTemp + 1, atomic_concat(Id,N1, Nome),
			verificaLinhaProd(Prod,Linhas),
			((isEmpty(Linhas),!,true);
			(getLinhaMenosCarregada(Linhas, Linha, _), atomic_concat(Id,N1,Nome), assertz(tarefa(TConc, Nome, Make, Prioridade,Linha, Prod)))).


% ----------------- CRIAÇÃO OPERAÇÕES ----------------------- %

% cria_op_enc - fizeram-se correcoes face a versao anterior


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)).



:-dynamic geracoes/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.
:-dynamic temp_lim_abs/1.
:-dynamic valor_especifico/1.
:-dynamic estabilizacao_geracoes/1.
:-dynamic tempo_inicial/1.
:-dynamic nrPopEstabilizadas/1.

% tarefa(TempConc,Id,Makespan,PesoPenalizacao, Linha).

% tarefas(NTarefas).

% ------------------------------ ALGORITMO GENÉTICO ------------------ %

% parameteriza��o
inicializa:-InitVar is 0,
	(retract(nrPopEstabilizadas(_));true), asserta(nrPopEstabilizadas(InitVar)),
	write('Numero de novas Geracoes: '),read(NG),
	(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100,
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100,
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
  write('Tempo Limite Absoluto (segundos): '), read(PTempo),
  (retract(temp_lim_abs(_));true), asserta(temp_lim_abs(PTempo)),
  write('Valor da avaliação específica: '),read(PValorEsp),
  (retract(valor_especifico(_));true), asserta(valor_especifico(PValorEsp)),
  write('Estabilização ao fim de N geracoes: '),read(PEstabilizacao),
  (retract(estabilizacao_geracoes(_));true), asserta(estabilizacao_geracoes(PEstabilizacao)),
    get_time(Tinicio),(retract(tempo_inicial(_));true), asserta(tempo_inicial(Tinicio)). % get tempo inicial depois de todos os inputs


geraSemInput(Geracoes, PopNum, Cruzamento, Mutacao, TempoLim, ValorEsp, Estabilizacao):- InitVar is 0,
			(retract(nrPopEstabilizadas(_));true), asserta(nrPopEstabilizadas(InitVar)),
			(retract(geracoes(_));true), asserta(geracoes(Geracoes)), (retract(populacao(_));true), asserta(populacao(PopNum)),
			(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(Cruzamento)),
			(retract(prob_mutacao(_));true), asserta(prob_mutacao(Mutacao)),(retract(temp_lim_abs(_));true), asserta(temp_lim_abs(TempoLim)),
			 (retract(valor_especifico(_));true), asserta(valor_especifico(ValorEsp)),
			 (retract(estabilizacao_geracoes(_));true), asserta(estabilizacao_geracoes(Estabilizacao)),
			 get_time(Tinicio),
			 (retract(tempo_inicial(_));true), asserta(tempo_inicial(Tinicio)),
        gera_populacao(Pop),
	avalia_populacao(Pop,PopAv),
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	gera_geracao(0,NG,PopOrd).
gera:-
	inicializa,
	gera_populacao(Pop),
	write('Pop='),write(Pop),nl,
	avalia_populacao(Pop,PopAv),
	write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	gera_geracao(0,NG,PopOrd).
gera_populacao(PopFinal):-
	populacao(TamPop),
	TamPopSemHeuristica is TamPop - 2,
	tarefas(NumT),
	findall(Tarefa,tarefa(_,Tarefa,_,_,_,_),ListaTarefas),
	gera_populacao(TamPopSemHeuristica,ListaTarefas,NumT,Pop),
        adicionarHeuristicaEdd(Pop, NovaPop),
	adicionarHeuristicaSlack(NovaPop, PopFinal).

adicionarHeuristicaEdd(Pop, [Heuristica|Pop]):- h_m_tempo_atraso_edd(Heuristica).

adicionarHeuristicaSlack(Pop, [Heuristica|Pop]):- h_m_min_slack(Heuristica).

gera_populacao(0,_,_,[]):-!.

gera_populacao(TamPop,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaTarefas,NumT,Resto),
	gera_individuo(ListaTarefas,NumT,Ind),
	not(member(Ind,Resto)).
gera_populacao(TamPop,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,ListaTarefas,NumT,L).

gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).

avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(Prazo,T,Dur,Pen,_,_),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	(
		(InstFim =< Prazo,!, VT is 1)
  ;
		(VT is (InstFim-Prazo)*Pen)
	),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).


btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).


gera_geracao(G,G,Pop):-!,
	devolvePrimeiro(Pop, PopF), criaPopIdeal(PopF), true.

gera_geracao(N,G,Pop):-
	melhoresInd(2,Pop,L), % 2 é o nr de melhores individuos da pop (Alínea 2)
	reverse(L,L1),
	random_permutation(Pop,PopRdm), % Alínea 3
	cruzamento(PopRdm,NPop1),
	mutacao(NPop1,NPop),
	avalia_populacao(NPop,NPopAv),
	append([L1,NPopAv],LRes), % lista c/ Pop+X melhores (valor do melhoresInd)
	random_permutation(LRes,LResRnd),
	torneio(2,LResRnd,LFinal),
	ordena_populacao(LFinal,NPopOrd),
	N1 is N+1,
	% Condições de Término
  get_time(Tatual),
  (((condicao_tempo(Tatual),
	condicao_valor_especifico(NPopOrd),
	condicao_estabilizacao_geracoes(Pop,NPopOrd),!,
	gera_geracao(N1,G,NPopOrd)));(true)).

condicao_tempo(Tatual):-
  tempo_inicial(Tinicial),
  temp_lim_abs(Tlimite),
  Texec is Tatual-Tinicial,
  ((Texec >= Tlimite,!,false);(true)).

condicao_valor_especifico([_*V|_]):-
  valor_especifico(Vesp),
  ((V>Vesp,!,true);(false)).

condicao_estabilizacao_geracoes(PopAnterior,PopAtual):-
  estabilizacao_geracoes(NumGera),
  nrPopEstabilizadas(NumAtual),
  ((checkIfListasIguais(PopAnterior,PopAtual),!,
   N is NumAtual+1,(retract(nrPopEstabilizadas(_));true),
  asserta(nrPopEstabilizadas(N)));
  ((retract(nrPopEstabilizadas(_));true),
  asserta(nrPopEstabilizadas(0)))),
  nrPopEstabilizadas(NumAtual1),
  ((NumAtual1=NumGera,!,false);(true)).

checkIfListasIguais(L1,L2):-
  %msort(L1,S1), %sort both lists
  %msort(L2,S2),
  subtract_custom(L1,L2,Res),
  not(member(_,Res)).

subtract_custom(Resto, [], Resto):- !.
subtract_custom(Lista, [Atual | Apagar], L) :-
    select(Atual, Lista, Resto),
    !,
    subtract_custom(Resto, Apagar, L).

unifiable([], _, []).

unifiable([First | Rest], Term, List) :-
  \+(First = Rest), !,
  unifiable(Rest, Term, List).

unifiable([First | Rest], Term, [First | List]) :-
  unifiable(Rest, Term, List).

torneio(0,Resto,Resto):-!. % Os que nao lutaram passam

torneio(N,[H|TLista],[HFinal|TFinal]):-
  N1 is N-1,
  torneio2(TLista,H,ListResto,HFinal),
  torneio(N1,ListResto,TFinal).

torneio2([Ind*V1|Tail],ElementoAnt*V2,Tail,Vencedor*X):-
  random(0,V1,Random1),
  random(0,V2,Random2),
  R1 is (Random1/(V1+V2)),
  R2 is (Random2/(V1+V2)),
  ((R1 < R2,!,Vencedor = Ind, X is V1);(Vencedor = ElementoAnt, X is V2)).



melhoresInd(0,_,_):-!.

melhoresInd(X,[HP|TP],[HP|L]):-
	X1 is X-1,
	melhoresInd(X1,TP,L).

gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).


% ----------------------- MakeSpan -----------------------

%Get operacao inicial com setup mais eficiente

getOperacaoInicialMakespan(Produto, Op):- findall(p(Texec,TSetup, Op,Qtd), op_prod_client(Op,_,_,Produto,_,Qtd,_,TSetup,Texec), LL),reverse(LL, LLRev), calculaMakespanSetup(LLRev,_, _,Op).

%Calcula o makespan em relação a um produto
calculaMakespan(Produto, MakespanFinal):- findall(p(Texec,TSetup, Op,Qtd), op_prod_client(Op,_,_,Produto,_,Qtd,_,TSetup,Texec), LL), sort(LL, Sorted),calculaMakespan2(Sorted,Makespan), reverse(LL, LLRev), calculaMakespanSetup(LLRev,_, Setup,_), MakespanFinal is Makespan - Setup.


calculaMakespan2([p(Texec,_,_,Qtd)], ResultadoFinal):- ResultadoFinal is Texec * Qtd.

calculaMakespan2([p(Texec,_,_,_)|Tail], ResultadoFinal):- calculaMakespan2(Tail,ResultadoFinal1), ResultadoFinal is ResultadoFinal1 + Texec.


calculaMakespanSetup([p(Texec,Tsetup,_,_)], TExecTotal, -Tsetup, _):-TExecTotal is Texec.
calculaMakespanSetup([p(Texec,TSetup, Op, _)|Tail], TExecTotal, Makespan, OpFinal):- calculaMakespanSetup(Tail, TExecTotal1, Makespan1,Op1), Temp is TExecTotal1 - TSetup, ((Temp<Makespan1,!, Makespan is Temp, OpFinal = Op);(Makespan is Makespan1, OpFinal = Op1)), TExecTotal is TExecTotal1+ Texec.

% Verifica se o produto é compatível com a linha
% Retira as máquinas a que foi atribuida o fabrico e verifica quais as
% linhas que contém as máquinas

verificaLinhaProd(Produto, ListaLinhas):- findall(Maq, op_prod_client(_,Maq,_,Produto,_,_,_,_,_), ListaMaq), verificarLinhas(ListaMaq, ListaLinhas).

verificarLinhas(Maquinas, LinhasF):- findall(Linha, (tipos_maq_linha(Linha, Maqs), listContainsMembers(Maquinas,Maqs)),LinhasF).

listContainsMembers(List1, List2):- percorrerLista(List1,List2).

percorrerLista([], _).
percorrerLista([H|T], List2):- percorrerLista(T, List2),member(H, List2).

% Get Linha menos sobrecarregada
% Vai buscar as tarefas correspondentes a cada linha e calcula o
% makespan acumulado.

getLinhaMenosCarregada([],l1,9999999).
getLinhaMenosCarregada([Linha|T], LinhaF, Make):- getLinhaMenosCarregada(T,Linha1, Make1),getMakespanLinha(Linha, MakeTemp), ((MakeTemp < Make1,!,LinhaF = Linha, Make is MakeTemp);(LinhaF = Linha1, Make is Make1)).

getMakespanLinha(L, Make):- findall(Makes, (tarefa(_,_,Makes,_,L,_)), ListaMakes), listSum(ListaMakes, Make).


% ------------------------ MÉTODOS AUXILIARES ------------------------%
% Soma items de lista

listSum([], 0).
listSum([H|T], Sum):- listSum(T, Sum1), Sum is Sum1 + H.

% Retira items da lista antes do elemento

eliminaItem(Elemento, [_|T], L):- eliminaItem(Elemento, T, L).
eliminaItem(Elemento, [Elemento|T], T).

% Adiciona items da lista antes do elemento

getItemsAntesElem(Elemento, Lista, L):- reverse(Lista, LRev), eliminaItem(Elemento, LRev, L).

% Apaga primeiro elem

apagaPrimeiro([_|T], T).

% Devolve primeiro elemento

devolvePrimeiro([H|_], H).

% Apaga primeiro e segundo

apagaPrimeiroSegundo([H|T], Final, Final2):- apagaPrimeiroSegundo2(H,T, Final, Final2).
apagaPrimeiroSegundo2(H1,[H|T], T, Final):- append([H1],[H], Final).

% Retira lista da avaliação

tiraAvaliacao(L*_, L).

% Verifica se está vazia
isEmpty([]).

% MaiorElemento
maiorElementoAgenda(Tf2, t(_,Tf2,exec,[_,_,_,_,_])).

maiorElementoAgenda(Tf, [t(_,Tf2,exec,[_,_,_,_,_])|T]):- maiorElementoAgenda(M1, T), (M1>Tf2, Tf is M1);Tf is Tf2.

remove_duplicates([],[]).

remove_duplicates([H | T], List) :-
     member(H, T),
     remove_duplicates( T, List).

remove_duplicates([H | T], [H|T1]) :-
      \+member(H, T),
      remove_duplicates( T, T1).

% permuta/2 gera permutações de listas
permuta([ ],[ ]).
permuta(L,[X|L1]):-apaga1(X,L,Li),permuta(Li,L1).

apaga1(X,[X|L],L).
apaga1(X,[Y|L],[Y|L1]):-apaga1(X,L,L1).


% ----------- Heuristica Earliest Due Date com Pesos Prioridade----------%

h_m_tempo_atraso_edd(Tarefas):- earliest_due_date(Tarefas).

% Vai buscar as operações e ordena-as por tempo de conclusão
% multiplicado pela prioridade.
earliest_due_date(ListaFinal):- findall(p(TConc,Id),( tarefa(TConcTemp,Id,_,Prioridade,_,_), TConc is TConcTemp * Prioridade),LL), sort(LL, ListaSort), retirarTConcTermos(ListaSort, ListaFinal).
retirarTConcTermos([], []).
retirarTConcTermos([p(_,Id)|Tail], [Id|ListaFinal]):- retirarTConcTermos(Tail, ListaFinal).

%------------- Heuristica Folga ------------------------

h_m_min_slack(Tarefas):- min_slack(Tarefas).

min_slack(ListaFinal):- findall(p(Id, TProcessamento,TConc), (tarefa(TConcTemp,Id,TProcessamento, Prioridade,_,_), TConc is TConcTemp * Prioridade), ListaTermos), ordenarPorFolga(ListaTermos,[], 0, _,ListaFinal).

ordenarPorFolga([],ListaVisitados, _,_, ListaVisitados):-!.
ordenarPorFolga(ListaTermos, ListaVisitados, TInicial, TTotalExec, ListaF):- descobreProximaTarefaFolga(ListaTermos,ListaVisitados,TInicial, TTotalExec, NovaTarefa,_),deleteById(ListaTermos, NovaTarefa,ListaTermosNova), ordenarPorFolga(ListaTermosNova, [NovaTarefa|ListaVisitados],TTotalExec, _, ListaF).

% Descobre a proxima tarefa com a menor folga, não estando o ID dessa
% presenta na ListaFinal.
descobreProximaTarefaFolga([],[],TInicial,TInicial,_,999).
descobreProximaTarefaFolga([], _,TInicial, TInicial,_, 999).
descobreProximaTarefaFolga([p(Id, TProcessamento, TConc)|Tail], ListaFinal,Tinicial, TTotal, NovaTarefa, MenorFolga):-
			descobreProximaTarefaFolga(Tail, ListaFinal,Tinicial, TTotal1,NovaTarefa1,MenorFolga1),
			((\+member(Id, ListaFinal),!,
			TTotalTemp is Tinicial + TProcessamento,
			FolgaTemp is abs(TConc - TTotalTemp),
			((FolgaTemp < MenorFolga1,!, MenorFolga is FolgaTemp, NovaTarefa = Id, TTotal is TTotalTemp)
			;(MenorFolga is MenorFolga1,NovaTarefa = NovaTarefa1, TTotal is TTotal1)));(MenorFolga is MenorFolga1,NovaTarefa = NovaTarefa1, TTotal is TTotal1)).

deleteById([], _, []).
deleteById([p(Id,_, _)|Tail], Id, ListaTermosNova):- deleteById(Tail, Id, ListaTermosNova).
deleteById([p(Id,TProc, TConc)|Tail], IdDif, [p(Id,TProc,TConc)|ListaTermosNova]):- deleteById(Tail, IdDif, ListaTermosNova).


%- IMPLEMENTATION HTTP SERVER -%
%% HTTP
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_header)).
:- use_module(library(http/http_cors)).
:- use_module(library(http/http_unix_daemon)).
:- initialization(run, main).

run:-
    current_prolog_flag(argv, Argv),
    argv_options(Argv, _RestArgv, Options),
    http_daemon(Options), server(9091).

% CORS
:- set_setting(http:cors, [*]).
% JSON
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json)).

% REQUESTS
:- http_handler('/consultaPlanoProducao', get_plano_pp, []).
:- http_handler('/planoProducao', ativa_plano_pp, []).
:- http_handler('/agendasTemporaisSetup', agendas_pp, []).
:- http_handler('/agendasTemporaisExec', agendas_exec_pp, []).


% CONFIG
jsonType('Content-type: application/json~n~n').


% SERVER
server(P):-
        http_server(http_dispatch, [port(P)]).

stopServer(P):- http_stop_server(P,_).

% REQUESTS RESPONSE

agendas_pp(Request):- cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),findall(a(Maq,AgendasF), (agenda_maq(Maq, Agendas),sort(Agendas,AgendasF)), ListaAgendas), criaJsonAgendasSetup(ListaAgendas, Json), prolog_to_json(Json, JsonR), reply_json(JsonR, [json_object(dict)]).

agendas_exec_pp(Request):- cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),findall(a(Maq,Agendas), agenda_maq(Maq, Agendas), ListaAgendas), criaJsonAgendasExec(ListaAgendas, Json), prolog_to_json(Json, JsonR), reply_json(JsonR, [json_object(dict)]).

get_plano_pp(Request):-cors_enable(Request,
                  [ methods([get,post,delete])
                  ]), ((planoFabrico(Id, ListaLotes, TempoConc),!, getTarefas(ListaLotes,Lista),R=json([idPlano=Id,tempoExecucao=TempoConc, ordens=Lista]), prolog_to_json(R, Json),reply_json(Json, [json_object(dict)]));(R=json([]), reply_json(R,[json_object(dict)]))).

ativa_plano_pp(Request):- http_parameters(Request,
                    [ date(Data, [])
                    ]),geraEscalonamentoComDadosReais(Data), planoFabrico(Id, ListaLotes, TempoConc), getTarefas(ListaLotes,Lista),R=json([idPlano=Id,tempoExecucao=TempoConc, ordens=Lista]), prolog_to_json(R, Json),reply_json(Json, [json_object(dict)]).

getTarefas(ListaLotes, ListaJson):- getLotesJson(ListaLotes,ListaJson).

getLotesJson([], []).
getLotesJson([Lote|Tail], [R|ListaJson]):- getLotesJson(Tail, ListaJson), tarefa(TConc, Lote,Makespan,Prioridade,Linha,Prod), R=json([idLote=Lote,tempoConc=TConc,makespan=Makespan,prioridade=Prioridade,idLinha=Linha,idProd=Prod]).


criaJsonAgendasExec([], []).
criaJsonAgendasExec([a(Maq, Agendas)|Tail], [R|ListaJson]):- criaJsonAgendasExec(Tail, ListaJson), retiraSetups(Agendas, _, ListaExec),getJsonAgendasExec(ListaExec, Json), R=json([maq=Maq, agendas=Json]).

criaJsonAgendasSetup([], []).
criaJsonAgendasSetup([a(Maq, Agendas)|Tail], [R|ListaJson]):- criaJsonAgendasSetup(Tail, ListaJson), retiraSetups(Agendas, ListaSet, _),getJsonAgendasSetup(ListaSet, Json), R=json([maq=Maq, agendas=Json]).

getJsonAgendasSetup([], []).
getJsonAgendasSetup([t(Ti,Tf,setup,[Ferr])|Tail], [R|ListaFinal]):-getJsonAgendasSetup(Tail, ListaFinal), R=json([tempoInicial=Ti, tempoFinal=Tf, modo=setup, ferr=Ferr]).

getJsonAgendasExec([], []).
getJsonAgendasExec([t(Ti,Tf,exec, [Op, Qtd, Prod, Cl,Tarefa])|Tail], [R|ListaFinal]):- getJsonAgendasExec(Tail, ListaFinal), R=json([tempoInicial=Ti, tempoFinal=Tf, modo=exec, operacao=Op, quantidade=Qtd, prod=Prod, cliente=Cl, lote=Tarefa]).
% URLS

%geUrl('https://lapr5-3de-nodejs.herokuapp.com/api/encomendas/ativas/').
geUrl('https://lapr5-3de-nodejs.herokuapp.com/api/encomendas/ativas/').
geUrlCliente('https://lapr5-3de-nodejs.herokuapp.com/api/users/').
mdfUrlLinha('https://lapr5-3de-02-mdf.azurewebsites.net/api/LinhaProducao').
mdfUrlTipoMaq('https://lapr5-3de-02-mdf.azurewebsites.net/api/TipoMaquina/').
mdfUrlMaquina('https://lapr5-3de-02-mdf.azurewebsites.net/api/Maquina/ativas').
mdfUrlOperacao('https://lapr5-3de-02-mdf.azurewebsites.net/api/Operacao/').
mdpUrlProduto('https://lapr5-3de-02-mdp.azurewebsites.net/api/Produto/').
token('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkZmM4ZDNmODgxZjNkMDAzNWJlYzUwYiIsInVzZXJuYW1lIjoiYWRtaW4iLCJyb2xlIjoiQWRtaW4iLCJpYXQiOjE1Nzg2NTY4MjEsImV4cCI6MTU3ODc0MzIyMSwiaXNzIjoiaHR0cHM6Ly9teW93bmN1dGxlcnktbGFwcjUtM2RlLTAyLmhlcm9rdWFwcC5jb20vIn0.SQFZqO6N_idSUGXIUZOKjEJee7CioK_RKYvd2SaEUOM').
% GET DATA FROM MDF MDP

geraDadosSemOutput(Data):- getOperacoes, getMaquinas, getProdutos, getLinhas, getClientes, getEncomendasData(Data).

geraDados:- write('Procurando operações........\n'),getOperacoes, write('Operações adicionadas com sucesso.............\n'),
			write('Procurando máquinas ativas e tipos de máquina........\n'), getMaquinas,write('Máquinas e tipos de máquina adicionadas com sucesso.............\n'),
			write('Procurando produtos........\n'), getProdutos,write('Produtos adicionados com sucesso.............\n'),
			write('Procurando linhas de produção........\n'),
			getLinhas,write('Linhas de produção adicionadas com sucesso.............\n'),
			write('Procurando clientes........\n'),getClientes,write('Clientes adicionados com sucesso.............\n'),
			write('Procurando encomendas ativas........\n'),getEncomendasData("25-10-2019"), write('Encomendas adicionadas com sucesso.............\n').


% GET CURRENT DAY
day(Day) :-
    get_time(Stamp),
    stamp_date_time(Stamp, DateTime, local),
    date_time_value(day, DateTime, Day).

% ----- GET ENCOMENDAS -----%
% Predicado vai buscar as encomendas ao servidor GE e cria os factos
% encomenda/2. Verifica também se o cliente existe(se não
% existir cria) clientes/1 e prioridade_cliente/2.

getEncomendasData(Data):-
    geUrl(URL),
    atom_concat(URL, Data, URLF),
    token(T),
    http_get(URLF,Reply,[json_object(dict),(request_header(x-access-token=T))]),
    retractall(encomenda(_,_)),
    criarEncomendas(Reply).

criarEncomendas([]):-!.
criarEncomendas([Encomenda|Tail]):- verificaCliente(Encomenda.userId),converterDataEmStamp(Encomenda.dataEntrega, StampTConc),get_time(StampAtual), calculaDiferencaDatas(StampAtual, StampTConc, TConc), criarEncomendaFact(Encomenda.produtos,Encomenda.userId, TConc), criarEncomendas(Tail).

criarEncomendaFact(Produtos, UserId, TConc):- retirarProdutos(Produtos, ProdFinal,TConc), criarFactoEncomenda(UserId, ProdFinal).

retirarProdutos([], [],_).
retirarProdutos([Prod|Tail], [e(Id, Quantidade, TConc)|Lista], TConc):-retirarProdutos(Tail,Lista, TConc), Id = Prod.idProduto, Quantidade = Prod.quantidade.

criarFactoEncomenda(UserId, ProdFinal):-assertz(encomenda(UserId, ProdFinal)).

% ------ CLIENTE ------ %

getClientes:-geUrlCliente(URL),
    token(T),
    http_get(URL,Reply,[json_object(dict),(request_header(x-access-token=T))]),
    retractall(clientes(_)), assertz(clientes([])),
    criarCliente(Reply).

criarCliente([]).
criarCliente([Cliente|Tail]):- criarCliente(Tail),criarClienteFact(Cliente.id, Cliente.prioridade).

criarClienteFact(Id,Prioridade):- retract(clientes(Lista)), assertz(clientes([Id|Lista])), assertz(prioridade_cliente(Id, Prioridade)).

getClienteById(UserId,Reply):-
			geUrlCliente(URL),
			 token(T),
			atomic_concat(URL, UserId, URLFinal),http_get(URLFinal , Reply, [json_object(dict),(request_header(x-access-token=T))]).

verificaCliente(UserId):- clientes(Lista),
			((member(UserId, Lista),!,true);(retractall(clientes(_)), assertz(clientes([UserId|Lista])),getClienteById(UserId,Reply), assertPrioridade(UserId, Reply.prioridade))).

assertPrioridade(UserId, Prioridade):- assertz(prioridade_cliente(UserId,Prioridade)).


% ----- GET PRODUTOS ----- %
% Vai buscar os produtos e adiciona-o ao predicado produtos/1, criando
% tambem o operacoes_produto/2 com os dados do plano fabrico.
getProdutos:- mdpUrlProduto(URL), http_get(URL , Reply, [json_object(dict)]), criarProdutos(Reply, ListaIds), retractall(produtos(_)), assertz(produtos(ListaIds)).

criarProdutos([], []):-!.
criarProdutos([Produto|Tail], [Id|ListaIds]):- criarProdutos(Tail, ListaIds), Id = Produto.identificador, assertz(operacoes_produto(Produto.identificador, Produto.planoFabrico.listaIdOperacoes)).

% ----- GET MAQUINAS ----- %
% Vai buscar as maquinas e adiciona ao predicado maquinas/1. Verificando o tipo demáquina, adiciona as operações e a máquina a operacao_maquina/5.

getMaquinas:- mdfUrlMaquina(URL), http_get(URL, Reply, [json_object(dict)]), criaMaquinas(Reply, ListaIds), retractall(maquinas(_)), assertz(maquinas(ListaIds)).

criaMaquinas([], []):-!.
criaMaquinas([Maquina|Tail], [Id|ListaIds]):- criaMaquinas(Tail, ListaIds), Id = Maquina.idMaq, adicionaTipoMaquinaOperacoes(Maquina.idMaq, Maquina.tipoMaquina).

adicionaTipoMaquinaOperacoes(Id, IdTipoMaq):- getTipoMaquina(Id,IdTipoMaq).

getTipoMaquina(IdMaq, IdTipoMaq):- mdfUrlTipoMaq(URL),string_concat(URL, IdTipoMaq, URLFinal), http_get(URLFinal, Reply, [json_object(dict)]), criaTipo(IdMaq,Reply).

criaTipo(IdMaq,Tipo):- criarFactoComOperacoes(IdMaq, Tipo.ops).

criarFactoComOperacoes(_, []).
criarFactoComOperacoes(Id, [Op1|Tail]):- criarFactoComOperacoes(Id, Tail), ((operacao_maquina(Op1, Id,_,_,_),!,true);(getOperacaoById(Op1, Operacao), converterSecsToMinutes(Operacao.duracaoSetup, Set), converterSecsToMinutes(Operacao.duracao, Dur),assertz(operacao_maquina(Op1, Id, Operacao.ferramenta, Set, Dur)))).


% ----- GET OPERACOES ----- %
% Vai buscar as operações e adiciona cada uma ao predicado
% tipo_operacoes/1.

getOperacoes:- mdfUrlOperacao(URL), http_get(URL, Reply, [json_object(dict)]), criaOperacoes(Reply, ListaIds), retractall(tipo_operacoes(_)), assertz(tipo_operacoes(ListaIds)).

getOperacaoById(Id, Reply):- mdfUrlOperacao(URL), atomic_concat(URL, Id, URLFinal), http_get(URLFinal, Reply, [json_object(dict)]).

criaOperacoes([], []):-!.

criaOperacoes([Operacao|Tail], [Id|ListaIds]):- criaOperacoes(Tail, ListaIds), Id = Operacao.idOp.

% ----- GET LINHAS ----- %
% Vai buscar as linhas de produção e adiciona cada uma ao predicado
% linhas/1 e aos tipos_maq_linha/2.

getLinhas:- mdfUrlLinha(URL), http_get(URL, Reply, [json_object(dict)]), criaLinhas(Reply, ListaIds), retractall(linhas(_)), assertz(linhas(ListaIds)).

criaLinhas([], []):-!.
criaLinhas([Linha|Tail], [Id|ListaIds]):- criaLinhas(Tail, ListaIds), Id = Linha.identificador, criaFactoTiposMaqLinha(Linha.identificador, Linha.listaMaquinas).

criaFactoTiposMaqLinha(IdLinha, ListaMaq):- verificarMaquinaAtiva(ListaMaq, ListaMaqF), flatten(ListaMaqF, ListaMaqF2),((tipos_maq_linha(IdLinha, _),!,retract(tipos_maq_linha(IdLinha,_)), assertz(tipos_maq_linha(IdLinha, ListaMaqF2)));(assertz(tipos_maq_linha(IdLinha, ListaMaqF2)))).

verificarMaquinaAtiva([], []).
verificarMaquinaAtiva([Maq|Tail], [MaqFinal|Lista]):- verificarMaquinaAtiva(Tail, Lista), maquinas(ListaM), ((member(Maq, ListaM),!,MaqFinal = Maq);(MaqFinal = [])).

% ----------------------- DATE AUX -------------------- %
converterSecsToMinutes(Secs, Minutes):- Minutes is Secs /60.

%Calcula diferença datas
calculaDiferencaDatas(StampAtual, StampEntrega, StampMinutos):- StampDif is StampEntrega - StampAtual, StampMinutos is StampDif / 60.

%DiaSem Mes Dia Ano
converterDataEmStamp(Data, Stamp):- split_string(Data, " ", "", Lista), nth0(1, Lista, Mes), nth0(2, Lista, Dia), nth0(3,Lista,Ano), atom_number(Dia, DiaInt), atom_number(Ano, AnoInt), mes(Mes, MesInt), date_time_stamp(date(AnoInt, MesInt, DiaInt), Stamp).

% MONTHS
mes("Jan", 1).
mes("Feb", 2).
mes("Mar", 3).
mes("May", 4).
mes("Apr", 5).
mes("Jun", 6).
mes("Jul", 7).
mes("Aug", 8).
mes("Sep", 9).
mes("Oct", 10).
mes("Nov", 11).
mes("Dec", 12).
